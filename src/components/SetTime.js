import React, {useEffect, useState}from "react";
import Clock from "./TimeNow";
class SetTime extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            curTime: "",
            time: []
        }
    }

    getTime = () =>{
        let today = new Date(),
        curTime = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds()
        this.setState({curTime});
    }  
    onBtnClickAddTime = () => {
        this.getTime()
        this.setState({
            time: [...this.state.time, this.state.curTime]
        });
    }
    render(){
        return(
            <div>
                <p>List time</p>
                <Clock></Clock>
                <div>
                {
                        this.state.time.map((value, index) => {
                            return <p key={index}>{value}</p>
                        })
                    }
                </div>
                <button onClick={this.onBtnClickAddTime} >Add time</button>
            </div>
        )
    };
}

export default SetTime;